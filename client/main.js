import angular from 'angular';
import angularMeteor from 'angular-meteor';
import angularSanitize from 'angular-sanitize';
import checkout from '../imports/components/checkout/checkout';
import priceTable from '../imports/components/priceTable/priceTable';
import customer from '../imports/components/customer/customer';
import admin from '../imports/components/admin/admin';
import orderHistory from '../imports/components/orderHistory/orderHistory';
import rules from '../imports/components/rules/rules';
import ruleCreate from '../imports/components/ruleCreate/ruleCreate';
import '../lib/ads.js';
import '../lib/checkout.js';
import '../lib/rules.js';
import '../imports/startup/accounts-config.js';
import abc from 'angular-bootstrap-confirm';
import aub from 'angular-ui-bootstrap'

angular.module('pod', [
    angularMeteor,
    angularSanitize,
    checkout.name,
    priceTable.name,
    customer.name,
    admin.name,
    orderHistory.name,
    rules.name,
    ruleCreate.name,
    'accounts.ui',
    abc,
    aub,
    'ui.bootstrap'
]);