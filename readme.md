# POD Coding Exercise - Ads Checkout

## Tech Stack

- Front End : Angular, Bootstrap
- Back End  : Meteor
- Packager  : NPM
- Database  : MongoDB
- Testing   : MochaJS, ChaiJS


## Install

This apps using `meteor` to run.

Install `meteor` first if you haven't install it. Please visit meteor official page for more information. https://www.meteor.com/install

If meteor already installed, on the project directory run this command:

installing project dependency:
```shell
meteor npm install
```

run meteor:

```shell
meteor run
```

Meteor and other required packages will be installed. If the process is done and server already up and running:
```shell
...
...
App running at: http://localhost:3000/
```

Visit `http://localhost:3000/` from browser


## Testing

To run unit testing for the app, run this command:
```shell
meteor test --driver-package practicalmeteor:mocha
```

Visit `http://localhost:3000/` from browser to check the test result

That's all :)