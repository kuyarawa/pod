/**
 * Created by kuyarawa on 7/14/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './orderHistory.html';

import { Checkout } from '../../../lib/checkout.js';

class OrderHistoryCtrl {
    constructor($scope, $reactive) {
        $reactive(this).attach($scope);

        this.helpers({
            history() {
                if (Meteor.user() ? Meteor.user().profile.admin : false) {
                    return Checkout.find({}, {
                        sort: {
                            createdAt: -1
                        }
                    });
                } else {
                    return Checkout.find({customer: Meteor.user().username}, {
                        sort: {
                            createdAt: -1
                        }
                    });
                }
            },
            isAdmin() {
                return (Meteor.user() ? Meteor.user().profile.admin : false);
            }
        });
    }
}

export default angular.module('orderHistory', [
    angularMeteor
])
    .component('orderHistory', {
        templateUrl: 'imports/components/orderHistory/orderHistory.html',
        controller: ['$scope', '$reactive', OrderHistoryCtrl]
    });