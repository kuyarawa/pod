/**
 * Created by kuyarawa on 7/13/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './priceTable.html';

import { Ads } from '../../../lib/ads.js';

class PriceTableCtrl {
    constructor($scope, $reactive) {
        $reactive(this).attach($scope);

        this.helpers({
            ads() {
                return Ads.find({}, {
                    sort: {
                        createdAt: 1
                    }
                });
            }
        });

        this.resetNotification();
    }

    resetNotification() {
        this.notification = {
            error : 0,
            message : ''
        };
    }

    checkout(model) {
        this.resetNotification();

        let cart = [];

        _.each(model, function(value) {
            if (value.amount > 0) {
                cart.push(value);
            }
        });

        // check if amount is filled for at least 1 item
        if (cart.length > 0) {
            this.apply('processCheckout', [Meteor.user(), cart], (error, result) => {
                let er = 0;
                let msg;

                if (error) {
                    er = 1;
                    msg = error.message;
                } else {
                    _.each(model, function(value) {
                        value.amount = null;
                    });
                    msg = result;
                }

                this.notification = {
                    error : er,
                    message : msg
                };
            });

        } else {
            this.notification = {
                error : 1,
                message : 'Your cart is empty!'
            };
        }
    }
}

export default angular.module('priceTable', [
    angularMeteor
])
    .component('priceTable', {
        templateUrl: 'imports/components/priceTable/priceTable.html',
        controller: ['$scope', '$reactive', PriceTableCtrl]
    });