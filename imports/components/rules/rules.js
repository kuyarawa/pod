/**
 * Created by kuyarawa on 7/14/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './rules.html';

import { Rules } from '../../../lib/rules.js';

class RulesCtrl {
    constructor($scope, $reactive) {
        $reactive(this).attach($scope);

        this.helpers({
            rules() {
                return Rules.find({}, {
                    sort: {
                        createdAt: -1
                    }
                });
            }
        });
    }

    deleteRule(id) {
        this.apply('deleteRules', [id], (error, result) => {
            let er = 0;
            let msg;

            if (error) {
                er = 1;
                msg = error.message;
            } else {
                msg = result;
            }

            console.log({
                error : er,
                message : msg
            });
        });
    }
}

export default angular.module('rules', [
    angularMeteor
])
    .component('rules', {
        templateUrl: 'imports/components/rules/rules.html',
        controller: ['$scope', '$reactive', RulesCtrl]
    });