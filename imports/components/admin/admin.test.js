/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';

if (Meteor.isClient) {
    import { assert, expect } from 'meteor/practicalmeteor:chai';
    import 'angular-mocks';

    import admin from './admin';

    describe('admin', function() {
        let element;

        beforeEach(function() {
            let $compile;
            let $rootScope;

            window.module(admin.name);

            inject(function(_$compile_, _$rootScope_){
                $compile = _$compile_;
                $rootScope = _$rootScope_;
            });

            element = $compile('<admin></admin>')($rootScope.$new(true));
            $rootScope.$digest();
        });

        describe('component', function() {
            it('should be showing tabs panel', function() {
                assert.include(element[0].querySelector('.tab-content').innerHTML, 'tabpanel');
            });
            it('should have 3 tabs', function() {
                assert.equal(element[0].querySelector('.nav.nav-tabs').childElementCount, 3);
            });
        });
    });
}