/**
 * Created by kuyarawa on 7/13/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './admin.html';

class AdminCtrl {
    constructor($scope) {
        $scope.viewModel(this);

    }
}

export default angular.module('admin', [
    angularMeteor
])
    .component('admin', {
        templateUrl: 'imports/components/admin/admin.html',
        controller: ['$scope', AdminCtrl]
    });