/**
 * Created by kuyarawa on 7/14/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './ruleCreate.html';

import { Ads } from '../../../lib/ads.js';

class RuleCreateCtrl {
    constructor($scope, $reactive) {
        $reactive(this).attach($scope);

        this.helpers({
            ads() {
                return Ads.find({}, {
                    sort: {
                        createdAt: 1
                    }
                });
            },
            customers() {
                return Meteor.users.find({"profile.admin": false}, {
                    sort: {
                        username: 1
                    }
                });
            }
        });

        this.defaultData();
        this.resetNotification();
    }

    resetNotification() {
        this.notification = {
            error : 0,
            message : ''
        };
    }

    defaultData() {
        // model
        this.formData = {
            createdAt: null,
            customers: '',
            ads: '',
            condition: {
                buy: 0
            },
            promo: {
                free: 0,
                discount: 0
            }
        };
    }

    create(model) {
        this.resetNotification();

        if (model.customer && model.ads) {
            this.apply('processRules', [model], (error, result) => {
                let er = 0;
                let msg;

                if (error) {
                    er = 1;
                    msg = error.message;
                } else {
                    this.defaultData();
                    msg = result;
                }

                this.notification = {
                    error : er,
                    message : msg
                };
            });
        } else {
            this.notification = {
                error : 1,
                message : 'Please select customer and ads!'
            };
        }
    }
}

export default angular.module('ruleCreate', [
    angularMeteor
])
    .component('ruleCreate', {
        templateUrl: 'imports/components/ruleCreate/ruleCreate.html',
        controller: ['$scope', '$reactive', RuleCreateCtrl]
    });
