/**
 * Created by kuyarawa on 7/13/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './customer.html';

class CustomerCtrl {
    constructor($scope) {
        $scope.viewModel(this);

    }
}

export default angular.module('customer', [
    angularMeteor
])
    .component('customer', {
        templateUrl: 'imports/components/customer/customer.html',
        controller: ['$scope', CustomerCtrl]
    });