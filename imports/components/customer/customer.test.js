/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';

if (Meteor.isClient) {
    import { assert, expect } from 'meteor/practicalmeteor:chai';
    import 'angular-mocks';

    import customer from './customer';

    describe('customer', function() {
        let element;

        beforeEach(function() {
            let $compile;
            let $rootScope;

            window.module(customer.name);

            inject(function(_$compile_, _$rootScope_){
                $compile = _$compile_;
                $rootScope = _$rootScope_;
            });

            element = $compile('<customer></customer>')($rootScope.$new(true));
            $rootScope.$digest();
        });

        describe('component', function() {
            it('should be showing <price-table>', function() {
                assert.include(element[0].innerHTML, '<price-table>');
            });
            it('should be showing <order-history>', function() {
                assert.include(element[0].innerHTML, '<order-history>');
            });
        });
    });
}