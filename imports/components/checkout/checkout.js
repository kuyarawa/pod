/**
 * Created by kuyarawa on 7/13/17.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './checkout.html';

class CheckoutCtrl {
    constructor($scope) {
        $scope.viewModel(this);

        this.helpers({
            currentUser() {
                return Meteor.user();
            },
            isAdmin() {
                return (Meteor.user() ? Meteor.user().profile.admin : false);
            }
        })
    }
}

export default angular.module('checkout', [
    angularMeteor
])
    .component('checkout', {
        templateUrl: 'imports/components/checkout/checkout.html',
        controller: ['$scope', CheckoutCtrl]
    });