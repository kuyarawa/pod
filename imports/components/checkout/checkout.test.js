/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';

if (Meteor.isClient) {
    import { assert, expect } from 'meteor/practicalmeteor:chai';
    import { sinon } from 'meteor/practicalmeteor:sinon';
    import 'angular-mocks';

    import checkout from './checkout';

    describe('checkout', function() {
        let element;

        beforeEach(function() {
            let $compile;
            let $rootScope;

            window.module(checkout.name);

            inject(function(_$compile_, _$rootScope_){
                $compile = _$compile_;
                $rootScope = _$rootScope_;
            });

            element = $compile('<checkout></checkout>')($rootScope.$new(true));
            $rootScope.$digest();
        });

        describe('component', function() {
            it('should be showing login button on header', function() {
                assert.include(element[0].querySelector('header').innerHTML, 'login-buttons');
            });
            it('should be showing login help', function() {
                assert.include(element[0].querySelector('pre').innerHTML, 'Predefined Customer Account');
            });
        });

        describe('controller', function() {
            let controller;

            beforeEach(() => {
                sinon.stub(Meteor, 'call');
                controller = element.controller('checkout');
            });

            afterEach(() => {
                Meteor.call.restore();
            });

            it('currentUser', function() {
                assert.equal(controller.currentUser, null)
            });
        });
    });
}