/**
 * Created by kuyarawa on 7/13/17.
 */

import { Rules } from '../../../lib/rules';
import { Checkout } from '../../../lib/checkout';

Meteor.startup(function() {
    Meteor.methods({
        'processCheckout': function(user, cart) {
            let co = new CheckOut(Rules);

            try {
                co.co.customer = user.username;

                _.each(cart, function(c) {
                    // remove unwanted object
                    delete c['$$hashKey'];
                    c.customer = user.username;
                    c.promo = {
                        free: 0,
                        discount: 0
                    };
                    co.add(c);
                });

                co.save();
            } catch (e) {
                throw new Meteor.Error(500, e.message);
            }

            return 'Your order is placed!';
        }
    });
});

export class CheckOut {
    constructor(pricingRules) {
        this.rules = pricingRules;

        // set default data
        this.co = {
            createdAt: null,
            customer: null,
            total: 0,
            items: []
        };
    }

    add(item) {
        // get rules for each items
        let rules = this.rules.find({ customer: item.customer, ads: item.code }).fetch();

        item.appliedRules = [];
        // check each rule condition
        _.each(rules, function(r) {
            // if condition met, add rules id to applied rules
            if (item.amount >= r.condition.buy) {
                item.appliedRules.push(r._id);

                // free item promo
                if (r.promo.free > 0) {
                    // check if total amount is higher than condition + free
                    if (item.amount >= (r.condition.buy + r.promo.free)) {
                        // if true then customer pay amount - free item
                        item.amount = item.amount - r.promo.free;
                    }

                    // free item give here
                    item.promo.free += r.promo.free;
                }
                // discount promo
                if (r.promo.discount > 0) {
                    item.promo.discount += r.promo.discount
                }
            }
        });
        item.subTotal = item.amount * (item.price - item.promo.discount);
        this.co.items.push(item);
    }

    total() {
        // return sum of sub total
        return _.reduce(this.co.items, function(memo, num){ return memo + num.subTotal; }, 0);
    }

    save() {
        this.co.total = this.total();
        this.co.createdAt = new Date();

        // save to db
        Checkout.insert(this.co);
    }
}