/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import { CheckOut } from './methods';
import { Rules } from '../../../lib/rules';

if (Meteor.isServer) {
    let co = new CheckOut(Rules);

    describe('Checkout', () => {

        describe('method', () => {

            it('class CheckOut is defined', () => {
                assert.isDefined(co, 'CheckOut is neither `null` nor `undefined`');
            });

            it('CheckOut.add() is function', () => {
                assert.isFunction(co.add);
            });

            it('CheckOut.total() is function', () => {
                assert.isFunction(co.total);
            });

            it('CheckOut.save() is function', () => {
                assert.isFunction(co.save);
            });

            // it('can add item', () => {
            //     let mock = {
            //
            //     };
            //     co.add(mock);
            //     assert.isFunction(co.save);
            // });


        });
    });
}