/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import './methods';
import { Rules } from '../../../lib/rules';

if (Meteor.isServer) {

    describe('Rules', () => {

        describe('method', (done) => {

            beforeEach((done) => {
                Rules.remove({});
                let mock = {
                    createdAt: null,
                    customers: 'test',
                    ads: 'test',
                    condition: {
                        buy: 1
                    },
                    promo: {
                        free: 1,
                        discount: 0
                    }
                };

                // We need to wait until the method call is done before moving on, so we
                // use Mocha's async mechanism (calling a done callback)
                Meteor.call('processRules', mock, done);
            });

            it('can create new rule', () => {
                assert.equal(Rules.find().count(), 1);
            });

            it('can delete rule', () => {
                Meteor.call('deleteRules', Rules.findOne()._id);

                assert.equal(Rules.find().count(), 0);
            });

        });
    });
}