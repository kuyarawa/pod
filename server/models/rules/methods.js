/**
 * Created by kuyarawa on 7/14/17.
 */

import { Rules } from '../../../lib/rules';

Meteor.startup(function() {
    Meteor.methods({
        'processRules': function(data) {

            try {
                // validate data
                data.createdAt = new Date();
                data.condition.buy = (data.condition.buy === null ? 0 : data.condition.buy);
                data.promo.free = (data.promo.free === null ? 0 : data.promo.free);
                data.promo.discount = (data.promo.discount === null ? 0 : data.promo.discount);

                Rules.insert(data);
            } catch (e) {
                throw new Meteor.Error(500, e.message);
            }

            return 'New rule is saved!';
        },
        'deleteRules': function(id) {

            try {
                Rules.remove({_id: id}, true);
            } catch (e) {
                throw new Meteor.Error(500, e.message);
            }

            return 'Rule is deleted!';
        }
    });
});