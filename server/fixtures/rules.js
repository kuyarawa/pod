/**
 * Created by kuyarawa on 7/13/17.
 */
import { Rules } from '../../lib/rules.js';

Meteor.startup(function() {
    Meteor.methods({
        fixtures_rules() {
            if (!Meteor.fixtures)
                throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
            Rules.remove({});

            console.log("-- Rules");

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "unilever",
                "ads" : "classic",
                "condition" : {
                    "buy" : 2
                },
                "promo" : {
                    "free" : 1,
                    "discount" : 0
                }
            });

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "apple",
                "ads" : "standout",
                "condition" : {
                    "buy" : 1
                },
                "promo" : {
                    "free" : 0,
                    "discount" : 23
                }
            });

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "nike",
                "ads" : "premium",
                "condition" : {
                    "buy" : 4
                },
                "promo" : {
                    "free" : 0,
                    "discount" : 15
                }
            });

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "ford",
                "ads" : "classic",
                "condition" : {
                    "buy" : 4
                },
                "promo" : {
                    "free" : 1,
                    "discount" : 0
                }
            });

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "ford",
                "ads" : "standout",
                "condition" : {
                    "buy" : 1
                },
                "promo" : {
                    "free" : 0,
                    "discount" : 13
                }
            });

            Rules.insert({
                "createdAt" : new Date(),
                "customer" : "ford",
                "ads" : "premium",
                "condition" : {
                    "buy" : 3
                },
                "promo" : {
                    "free" : 0,
                    "discount" : 5
                }
            });

            console.log("-- 6 Rules added");
        },
    });
});