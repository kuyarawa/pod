/**
 * Created by kuyarawa on 7/13/17.
 */
import { Ads } from '../../lib/ads.js';

Meteor.startup(function() {
    Meteor.methods({
        fixtures_ads() {
            if (!Meteor.fixtures)
                throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
            Ads.remove({});

            console.log("-- Ads");

            Ads.insert({
                "createdAt" : new Date(),
                "code" : "classic",
                'name' : "Classic Ad",
                "price" : 269.99
            });

            Ads.insert({
                "createdAt" : new Date(),
                "code" : "standout",
                'name' : "Standout Ad",
                "price" : 322.99
            });

            Ads.insert({
                "createdAt" : new Date(),
                "code" : "premium",
                'name' : "Premium Ad",
                "price" : 394.99
            });

            console.log("-- 3 Ads added");
        },
    });
});