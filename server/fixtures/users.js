/**
 * Created by kuyarawa on 7/13/17.
 */
Meteor.startup(function() {
    Meteor.methods({
        fixtures_users() {
            if (!Meteor.fixtures)
                throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
            Meteor.users.remove({});

            console.log("-- Users");

            Accounts.createUser({
                username: 'admin',
                password: 'admin',
                profile: {
                    admin: true
                },
                identitites: {}
            });

            Accounts.createUser({
                username: 'unilever',
                password: 'password',
                profile: {
                    admin: false
                },
                identitites: {}
            });

            Accounts.createUser({
                username: 'apple',
                password: 'password',
                profile: {
                    admin: false
                },
                identitites: {}
            });

            Accounts.createUser({
                username: 'nike',
                password: 'password',
                profile: {
                    admin: false
                },
                identitites: {}
            });

            Accounts.createUser({
                username: 'ford',
                password: 'password',
                profile: {
                    admin: false
                },
                identitites: {}
            });

            Accounts.createUser({
                username: 'regular',
                password: 'password',
                profile: {
                    admin: false
                },
                identitites: {}
            });

            console.log("-- Users added (1 Admin, 5 Customer)");
        },
    });
});
