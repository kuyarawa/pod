/**
 * Created by kuyarawa on 7/13/17.
 */

import { Ads } from '../../lib/ads.js';

Meteor.startup(function() {
    Meteor.methods({
        build_fixtures() {
            if (Ads.find().count() === 0) {
                console.log("*** Building fixtures ***");
                Meteor.fixtures = true;
                Meteor.call('fixtures_users');
                Meteor.call('fixtures_ads');
                Meteor.call('fixtures_rules');
                Meteor.fixtures = false;
                console.log("*** Fixtures rebuild ***");
            }
        }
    });

    Meteor.call('build_fixtures');
});