/**
 * Created by kuyarawa on 7/14/17.
 */
/* eslint-env mocha */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';

import './ads';
import { Ads } from '../../lib/ads.js';
import './rules';
import { Rules } from '../../lib/rules.js';
import './users';

if (Meteor.isServer) {
    describe('Fixtures', () => {

        describe('User', () => {

            it('predefined users count is 6', () => {
                // set fixtures apply to true
                Meteor.fixtures = true;
                // run fixtures_ads;
                Meteor.call('fixtures_users');
                // reset fixtures apply to false
                Meteor.fixtures = false;

                assert.equal(Meteor.users.find().count(), 6);
            });

            it('predefined admin count is 1', () => {
                assert.equal(Meteor.users.find({"profile.admin": true}).count(), 1);
            });

            it('predefined customers count is 5', () => {
                assert.equal(Meteor.users.find({"profile.admin": false}).count(), 5);
            });

        });

        describe('Ads', () => {
            it('predefined ads count is 3', () => {

                // set fixtures apply to true
                Meteor.fixtures = true;
                // run fixtures_ads;
                Meteor.call('fixtures_ads');
                // reset fixtures apply to false
                Meteor.fixtures = false;

                assert.equal(Ads.find().count(), 3);
            });
        });

        describe('Rules', () => {
            it('predefined rules count is 6', () => {

                // set fixtures apply to true
                Meteor.fixtures = true;
                // run fixtures_ads;
                Meteor.call('fixtures_rules');
                // reset fixtures apply to false
                Meteor.fixtures = false;

                assert.equal(Rules.find().count(), 6);
            });
        });
    });
}