import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
    Accounts.onCreateUser(function (options, user) {
        user.profile = {
            admin: false
        };
        user.identities = {};

        return user;
    });
});
